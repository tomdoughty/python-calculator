import unittest
from index import shouldFrontFlip, shouldAddCStar, getFrontOrBackflipScores, backflipGcseScores, frontflipGcseScores, normaliseGrade, getValidGrades, getScore, getTotalScore, scoreObject

fullBackflipScores = getFrontOrBackflipScores(2018, 'England')
fullFrontflipScores = getFrontOrBackflipScores(2019, 'England')

class TestGcseCalculator(unittest.TestCase):
	def test_shouldFrontFlip(self):
		self.assertTrue(
			shouldFrontFlip(2019, 'England'),
			'returns true for students in 2019 onwards in England')

		self.assertFalse(
			shouldFrontFlip(2018, 'England'), 
			'returns false for students before 2019 in England')
		
		self.assertFalse(
			shouldFrontFlip(2025, 'Jamaica'), 
			'returns false for any students not in England')


	def test_shouldAddCStar(self):
		self.assertTrue(
			shouldAddCStar(2018, 'Northern Ireland'), 
			'returns true for students in 2018 onwards in Northern Ireland')

		self.assertFalse(
			shouldAddCStar(2017, 'Northern Ireland'), 
			'returns false for students in before 2018 in Northern Ireland')
		
		self.assertFalse(
			shouldAddCStar(2020, 'Jamaica'), 
			'returns false for any students not in Northern Ireland')


	def test_getFrontOrBackflipScores(self):
		scores = getFrontOrBackflipScores(2018, 'England')['standardScores']
		self.assertEqual(
			scores, 
			backflipGcseScores, 
			'returns backflip scores for students up to 2018/19 in England')

		scores = getFrontOrBackflipScores(2020, 'Wales')['standardScores']
		self.assertEqual(
			scores, 
			backflipGcseScores, 
			'returns backflip scores for students in any academic year outside of England')

		scores = getFrontOrBackflipScores(2018, 'Northern Ireland')['standardScores']
		self.assertTrue(
			'C*' in scores, 
			'contains C* for students in 2018/2019 onwards in Northern Ireland')

		scores = getFrontOrBackflipScores(2019, 'England')['standardScores']
		self.assertEqual(
			scores, 
			frontflipGcseScores, 
			'returns frontflip scores for students in 2019/2020 in England')

	def test_normaliseGrade(self):
		self.assertEqual(
			normaliseGrade('abc'), 
			'ABC', 
			'returns string in uppercase')

		self.assertEqual(
			normaliseGrade(' ABC'), 
			'ABC', 
			'trims leading spaces')

		self.assertEqual(
			normaliseGrade('ABC '), 
			'ABC', 
			'trims trailing spaces')

		self.assertEqual(
			normaliseGrade(' ABC '), 
			'ABC', 
			'trims leading and trailing spaces')


	def test_getValidGrades(self):
		self.assertEqual(
			getValidGrades(['A', 'B', 'Z', 'C', '9', 'ABC'], backflipGcseScores),
			['A', 'B', 'C', '9'], 
			'returns an array of only valid grades')


	def test_getScore(self):
		self.assertEqual(
			getScore([], backflipGcseScores),
			scoreObject,
			'returns a default score object if inputs array is empty')
		
		self.assertEqual(
			getScore(['N', 'M', ''], backflipGcseScores),
			scoreObject, 
			'returns a default score object if no inputs are valid')
		
		self.assertEqual(
			getScore(['A', 'B', 'C'], frontflipGcseScores),
			{'score': 16.5, 'length': 3},
			'returns average score based on frontflip standard scores')

		self.assertEqual(
			getScore(['A', 'B', 'C'], backflipGcseScores),
			{'score': 18, 'length': 3},
			'returns average score based on backflip standard scores')

		self.assertEqual(
			getScore(['A', 'B', 'C'], fullBackflipScores['shortScores']),
			{'score': 9, 'length': 3},
			'returns average score based on frontflip short scores')

		self.assertEqual(
			getScore(['A', 'B', 'C', 'Z', ''], frontflipGcseScores),
			{'score': 16.5, 'length': 3},
			'ignores invalid grades when calculating score')


	def test_getTotalScore(self):
		self.assertEqual(
			getTotalScore(
				['A', 'B', 'C'],
				['A', 'B', 'C'],
				fullFrontflipScores
			),
			8.25,
			'calculates standard and short scores from inputs')
		
		self.assertEqual(
			getTotalScore(
				['A', 'B', 'C'],
				[''],
				fullFrontflipScores
			),
			5.5,
			'calculates only standard scores if passed no values in short grade inputs')
		
		self.assertEqual(
			getTotalScore(
				[''],
				['A', 'B', 'C', 'A', 'B', 'C'],
				fullFrontflipScores
			),
			5.5,
			'calculates only short scores if passed no values in standard grade inputs')

		self.assertEqual(
			getTotalScore(
				['A', 'B', 'C'],
				['A', 'B', 'C'],
				fullFrontflipScores
			),
			8.25,
			'calculates standard and short scores if both are passed')

		self.assertEqual(
			getTotalScore(
				['V'], ['P'], 
				fullFrontflipScores
			),
			0,
			'returns be zero if all grades are invalid')