from functools import reduce
import pandas as pd

"""Score object

@typedef {Object} scoreObject
@property {number} score
@property {number} length
"""
scoreObject = {
	'score': 0,
	'length': 0
}


"""Backflip scores in grade:score pairs"""
backflipGcseScores = {
	'*': 8,
	'A': 7,
	'B': 6,
	'C': 5,
	'D': 4,
	'E': 3,
	'F': 2,
	'G': 1,
	'U': 0,
	'9': 8,
	'8': 7.67,
	'7': 7,
	'6': 6.33,
	'5': 5.67,
	'4': 5,
	'3': 4,
	'2': 3,
	'1': 1
}

"""Frontflip scores in grade:score pairs"""
frontflipGcseScores = {
	'*': 8.5,
	'A': 7,
	'B': 5.5,
	'C': 4,
	'D': 3,
	'E': 2,
	'F': 1.5,
	'G': 1,
	'U': 0,
	'9': 9,
	'8': 8,
	'7': 7,
	'6': 6,
	'5': 5,
	'4': 4,
	'3': 3,
	'2': 2,
	'1': 1
}


"""Should scores be frontflipped based on ac year and country
Decide if the given year group in the given academic year should use front flipped grades

@param {number} options.academicYear - Academic year of student
@param {number} options.country - Country of client
@returns {boolean} Returns true if front flipped grades should be used, else false
"""
def shouldFrontFlip(academicYear = 0, country = ''):
	if (country.lower() == 'england'):
		if (academicYear >= 2019):
			return True
	return False
	

"""Should C* Grade be added to scores based on ac year and country

@param {Object} options - Options object containing required data for calculation
@param {number} options.academicYear - Academic year of student
@param {number} options.country - Country of client
@return {boolean}
"""
def shouldAddCStar(academicYear = 0, country = ''):
	if (country.lower() == 'northern ireland'):
		if (academicYear >= 2018):
			 return True
	return False

"""Return passed grades with their scores halved

@param {dict} grades - Object of grade:score pairs
@return {dict} - Object of grade:score pairs
"""
def getShortScores(grades):
	return reduce(
		(lambda allGrades, grade:
			{
				**allGrades,
				grade: grades[grade] * 0.5
			}
		),
		grades, 
		{}
	)


"""Get a new object with scores halved

@param {number} academicYear - Academic year start year to get grades for
@param {string} country - Country to get grades for
@return {object} - Standard and short course grades
"""
def getFrontOrBackflipScores(academicYear = 0, country = ''):
	# Default is to use backflip scores
	grades = backflipGcseScores
	# Get frontflip scores if needed
	if (shouldFrontFlip(academicYear, country)):
		grades = frontflipGcseScores
	# Add C* if needed
	if (shouldAddCStar(academicYear, country)):
		grades = { **grades, 'C*': 5.5 }
	return {
		'standardScores': grades,
		'shortScores': getShortScores(grades)
	}


"""Converts string to uppercase, trims leading and trailing spaces

@param {string} grade - String to be transformed
@return {string} An uppercase trimmed string
"""
def normaliseGrade(grade): return grade.strip().upper()


"""Checks if grades are valid and removes them if not

@param {string[]} grades - Array of grades to validate
@param {scoresObj} scoresObj - Scores object validate grades against
@return {string[]} Array of valid grades
"""
def getValidGrades(grades, scoresObj):
	return list(filter((lambda grade: grade in scoresObj), grades))


"""Get average GCSE Score based on double / standard scores

@param {gradeInput[]} inputs - Array of gradeInputs to process
@param {object} scoresObj - Object to get scores from in grade:score pairs
@return {scoreObject} Average GCSE Score
"""
def getScore(grades, scoresObj):

	validGrades = getValidGrades(grades, scoresObj)

	# Return 0 immediately if no grades are valid
	if (len(validGrades) == 0): return scoreObject

	# Get the score for each valid grade
	scores = list(map((lambda grade: scoresObj[grade]), validGrades))

	# Return a total of the scores and amount that were counted
	return dict(reduce(
		(lambda totalScore, score: {
			'score': totalScore['score'] + score,
			'length': len(scores)
		}),
		scores,
		scoreObject))


"""Get average GCSE Score based on double / standard scores

@param {gradeInput[]} standardInputs - Array of standard gradeInputs to process
@param {gradeInput[]} shortInputs - Array of short gradeInputs to process
@return {number} Average GCSE Score
"""
def getTotalScore(standardInputs, shortInputs, grades):
	standardScores = grades['standardScores']
	shortScores = grades['shortScores']

	# Return 0 if less than 3 full courses
	if ((len(standardInputs) + (len(shortInputs)*0.5)) < 3 ): return 0

	# Get total standard scores and length amount of short scores
	standard = getScore(standardInputs, standardScores)

	# Get total short score and length amount of short scores
	short = getScore(shortInputs, shortScores)

	# Get average of standard and short scores
	try : 
		score = (standard['score'] + short['score']) / (standard['length'] + (short['length']/2))
	# Catch error where lengths are zero
	except ZeroDivisionError:
			score = 0

	# Return rounded score
	return round(score, 2)


"""Build data frame containing student scores and save to CSV
@return CSV File
"""
def saveExcelOutput():

	df1 = pd.read_csv('./data/input.csv')
	students = {}

	for i in df1.index:
		# Store variables from each row
		studentId = str(df1.at[i, 'StudentId'])
		grade = str(df1.at[i, 'Grade'])

		# Catch results we don't want to process
		if str(df1.at[i, 'IsNumericGrade']) == 'NULL' or grade == 'U': continue

		# Set course type
		courseType = 'short' if df1.at[i, 'ExamTypePrefix'] == 'GCSE SC' else 'standard'

		# Copy of student or empty dictionary if student does not exist
		studentClone = students.get(studentId, {})

		students[studentId] = {
			# Default empty lists for standard and short scores
			** {
				'standard': [],
				'short': []
			},
			courseType: [
				# Keep all existing grades for course type
				*studentClone.get(courseType, []),
				# Create entry for each grade in grades list
				*list(map(lambda value: normaliseGrade(value), list(str(grade))))
			]
		}

	# Set variables for backflip and frontflip dictionaries 
	fullBackflipScores = getFrontOrBackflipScores(2018, 'England')
	fullFrontflipScores = getFrontOrBackflipScores(2019, 'England')

	# Used by lambda to build list of lists
	def getStudentsScores(student):
		studentDict = students[student]
		return [
			student, 
			getTotalScore(
				studentDict['standard'],
				studentDict['short'],
				fullBackflipScores
			),
			getTotalScore(
				studentDict['standard'],
				studentDict['short'],
				fullFrontflipScores
			)
		]

	pd.DataFrame(
		# Create data frame with a list of lists
		list(map(lambda student: getStudentsScores(student), list(students))),
		# Pass columns headers
		columns=['StudentId', 'GcseScoreBackflip', 'GcseScoreFrontflip']
	).to_csv('./data/output.csv')

saveExcelOutput()