# Python GCSE Calculator

Python implementation of GCSE Calculator, using Python 3.

## Run code

Run in command line: `python3 index.py`

## Run tests

Run in command line: `python3 -m unittest -v test.py`